// Реалізувати можливість зміни колірної теми користувача.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Взяти будь-яке готове домашнє завдання з HTML/CSS.
// Додати на макеті кнопку "Змінити тему".
// При натисканні на кнопку - змінювати колірну гаму сайту 
// (кольори кнопок, фону тощо) на ваш розсуд. При повторному натисканні - 
// повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// Вибрана тема повинна зберігатися після перезавантаження сторінки

// Шукаємо всі елементи, яким потрібно додати клас light (перелічуємо черех кому і весь внутрішній рядок в лапках)
let selectionElements = document.querySelectorAll("body, body > div, body > div *");
console.log(selectionElements);

// Додаємо всім вищезнайденим клас light методом перебору forEach
selectionElements.forEach((element) => element.classList.add("light"));

console.log(window.getComputedStyle(document.body).getPropertyValue("background-color"));

let light = document.createElement("button");
light.textContent = "Світла тема";
light.classList = "btn-theme";

let dark = document.createElement("button");
dark.textContent = "Темна тема";
dark.classList = "btn-theme";

let div = document.querySelector("header > img");
div.after(light, dark);

let btnTheme = document.querySelectorAll(".btn-theme");
console.log(btnTheme);
btnTheme.forEach((btn) => {
    btn.style.cssText = "display: flex; align-items: center; justify-content: center; border: 1px solid var(--color9); background-color: inherit; box-sizing: border-box; width: 218px; height: 50px; font-size: 16px; color: var(--color9)";
    btn.style.cursor = "pointer";
    btn.addEventListener('click', function(btn) {
        if (btn.target === light) {
            selectionElements.forEach((element) => element.classList.add("light"));
            localStorage.setItem("theme", "light" );
        } else if (btn.target === dark) {
            selectionElements.forEach((element) => element.classList.remove("light"));
            localStorage.setItem("theme", "dark");
        };
    });
})



let currentTheme = localStorage.getItem('theme');
console.log(currentTheme);
document.addEventListener("DOMContentLoaded", function() {
    // let currentTheme = localStorage.getItem('theme');
    console.log(currentTheme);
    if (currentTheme === "dark") {
        selectionElements.forEach((element) => element.classList.remove("light"));
    } else {
        selectionElements.forEach((element) => element.classList.add("light"));
    }
})

